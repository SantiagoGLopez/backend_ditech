﻿using Microsoft.AspNetCore.Mvc;
using PruebaDitech.Data;
using PruebaDitech.Models;

namespace PruebaDitech.Controllers
{
    [ApiController]
    [Route("api/seller")]
    public class SellerController : Controller
    {
        private readonly PruebaDbContext _db;

        public SellerController(PruebaDbContext pruebaDbContext)
        {
            _db = pruebaDbContext;
        }

        [HttpGet]
        public IActionResult GetAllSellers()
        {
            var sellers = _db.Seller.ToList();

            return Ok(sellers);
        }

        [HttpPost]
        public IActionResult AddSeller([FromBody] Seller seller)
        {
            var city = _db.City.Find(seller.city_id);

            if (city == null)
            {
                return NotFound("La ciudad no existe");
            }

            _db.Seller.Add(seller);
            _db.SaveChanges();

            return Ok("Se ha agregado el vendedor");
        }

        [HttpPut]
        public IActionResult EditSeller([FromBody] Seller updateSeller)
        {
            var seller = _db.Seller.Find(updateSeller.code);
            var city = _db.City.Find(updateSeller.city_id);

            if (seller == null || city == null)
            {
                return NotFound("El vendedor o la ciudad no existen");
            }

            seller.name = updateSeller.name;
            seller.last_name = updateSeller.last_name;
            seller.document = updateSeller.document;
            seller.city_id = updateSeller.city_id;

            _db.SaveChanges();

            return Ok(seller);
        }

        [HttpDelete]
        [Route("{code}")]
        public IActionResult DeleteSeller([FromRoute] int code)
        {
            var seller = _db.Seller.Find(code);

            if (seller == null)
            {
                return NotFound();
            }

            _db.Seller.Remove(seller);
            _db.SaveChanges();

            return Ok("Se ha eliminado el vendedor");
        }
    }
}
