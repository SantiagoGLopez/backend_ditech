﻿using Microsoft.AspNetCore.Mvc;
using PruebaDitech.Data;
using PruebaDitech.Models;

namespace PruebaDitech.Controllers
{
    [ApiController]
    [Route("api/city")]
    public class CityController : Controller
    {
        private readonly PruebaDbContext _db;

        public CityController(PruebaDbContext pruebaDbContext)
        {
            _db = pruebaDbContext;
        }

        [HttpGet]
        public IActionResult GetAllCities()
        {
            var cities = _db.City.ToList();

            return Ok(cities);

        }

        [HttpPost]
        public IActionResult AddCity([FromBody] City city)
        {
            _db.City.Add(city);
            _db.SaveChanges();

            return Ok("Se ha agregado la ciudad");
        }

        [HttpPut]
        public IActionResult EditCity([FromBody] City updateCity)
        {
            var city = _db.City.Find(updateCity.code);

            if (city == null)
            {
                return NotFound();
            }

            city.description = updateCity.description;

            _db.SaveChanges();

            return Ok(city);
        }

        [HttpDelete]
        [Route("{code}")]
        public IActionResult DeleteCity([FromRoute] int code)
        {
            var city = _db.City.Find(code);

            var sellers = _db.Seller.Where(x => x.city_id == code).ToList();

            if (city == null)
            {
                return NotFound();
            }

            if (sellers.Count > 0)
            {
                return BadRequest("No se puede eliminar la ciudad porque hay vendedores asociados");
            }

            _db.City.Remove(city);
            _db.SaveChanges();

            return Ok("Se ha eliminado la ciudad");
        }
    }
}
