﻿using Microsoft.EntityFrameworkCore;
using PruebaDitech.Models;

namespace PruebaDitech.Data
{
    public class PruebaDbContext : DbContext
    {
        public PruebaDbContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<City> City { get; set; }
        public DbSet<Seller> Seller { get; set; }
    }
}
