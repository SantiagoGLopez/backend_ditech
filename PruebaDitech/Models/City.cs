﻿using System.ComponentModel.DataAnnotations;

namespace PruebaDitech.Models
{
    public class City
    {
        [Key]
        public int code { get; set; }
        [Required]
        public string description { get; set; }
    }
}
