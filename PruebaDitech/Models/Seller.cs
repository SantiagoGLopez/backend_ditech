﻿using System.ComponentModel.DataAnnotations;

namespace PruebaDitech.Models
{
    public class Seller
    {
        [Key]
        public int code { get; set; }
        [Required]
        public string name { get; set; }
        [Required]
        public string last_name { get; set; }
        [Required]
        public string document { get; set; }
        [Required]
        public int city_id { get; set; }
    }
}
